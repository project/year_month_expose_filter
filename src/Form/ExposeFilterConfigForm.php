<?php

/**
 * @file
 * Contains Drupal\year_month_expose_filter\Form\ExposeFilterConfigForm.
 */
namespace Drupal\year_month_expose_filter\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ExposeFilterConfigForm.
 *
 * @package Drupal\ExposeFilterConfigForm\Form
 */
class ExposeFilterConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'expose_filter_config_form.settings',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('expose_filter_config_form.settings');
    $form['expose_form_details'] = [
      '#type' => 'details',
      '#title' => t('Expose Form Details'),
      '#open' => TRUE,
    ];
    $form['expose_form_details']['form_machine_name'] = [
      '#type' => 'textfield',
      '#title' => t('Form machine Name'),
      '#default_value' => $config->get('form_machine_name'),
	  '#description' => 'Enter the expose form machine name like',
    ];
	$form['expose_form_details']['year_count'] = [
      '#type' => 'textfield',
      '#title' => t('Enter no of years'),
      '#default_value' => $config->get('year_count'),
	  '#description' => 'Enter the no of years you want to show ',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('expose_filter_config_form.settings')
      ->set('form_machine_name', $form_state->getValue('form_machine_name'))
	  ->set('year_count', $form_state->getValue('year_count'))
      ->save();
  }

}
