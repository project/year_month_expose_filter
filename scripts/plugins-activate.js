(function ($, Drupal, drupalSettings) {
  "use strict";
 Drupal.behaviors.caqModule = {                             
    attach: function (context, settings) { 
      $("#edit-year, #edit-month").change(function() {	
       $(this).parents("form").trigger("submit");
 });	   
    }                                                            
  }; 
})(jQuery, Drupal, drupalSettings);